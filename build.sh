#!/usr/bin/env bash

eval $(aws ecr get-login --no-include-email --region eu-west-1) &&

docker build -t 185640164939.dkr.ecr.eu-west-1.amazonaws.com/cloudformationdemo:$BUILD_NR docker
docker push 185640164939.dkr.ecr.eu-west-1.amazonaws.com/cloudformationdemo:$BUILD_NR
