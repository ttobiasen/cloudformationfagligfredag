#!/usr/bin/env bash

export PATH=$PATH:/var/jenkins_home/bin &&
for f in bash-my-aws/*-functions; do source $f; done &&

mkdir -p build &&
cd build &&
cp ../$TEMPLATE_TO_DEPLOY.json . &&
sed -i -e "s/{VERSION_TO_DEPLOY}/$VERSION_TO_DEPLOY/"  $TEMPLATE_TO_DEPLOY.json &&

# Check if the stack is allready deployed.
stacks > /tmp/stacks.txt
export PM_CREATED=$(grep -c $TEMPLATE_TO_DEPLOY /tmp/stacks.txt) &&

if [ "$PM_CREATED" == "0" ]; then
    echo "Creating cloud formation" &&
    stack-create $TEMPLATE_TO_DEPLOY.json
else
    echo "Updating cloud formation " &&
    stack-update $TEMPLATE_TO_DEPLOY.json
fi


